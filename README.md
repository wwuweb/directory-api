# directory-client

Faculty/Staff directory front end built with [Angular](https://angular.io) and [Angular Material](https://material.angular.io/).

## Getting started

Development workstation requirements:

* NodeJS (tested with NodeJS 16)
* npm (tested with npm 8)
* Angular CLI 13+ (tested with 13.1.2)
* JDK 8+ (tested with Temurin-17.0.1+12, only required for building docker images)

On the Mac you can use MacPorts to install these tools like this:

```bash
sudo port install nodejs16
sudo port install npm8
sudo port install openjdk17
```

In Windows, you may wish to use a package manager like `winget` or `chocolatey`.

To install Angular CLI, run:

```bash
npm install -g @angular/cli
```

After cloning this project, `cd` to its directory and run `npm install` to install dependencies.

## reCAPTCHA

The Faculty/Staff Directory uses [Google reCAPTCHA](https://www.google.com/recaptcha/about/) to prevent bots from scraping email addresses from the directory.
Angular client support for Google reCAPTCHA is provided by `ng-recaptcha`.

For client development, you'll need to create `src/app/configuration/development/configuration.json` that looks like this:

```json
{
  "recaptchaSiteKey": "<site key goes here>"
}
```

This key must match the key used by `directory-api`.
Ask Aoife for the `directorytest.wwu.edu` key.
Note that files in `src/app/configuration` are not tracked by Git.
Do not put reCAPTCHA site keys in files committed to Git.

`directory-client` deployments in containers put `configuration.json` in secrets and copy the secret into the application when the container starts.

## Development

To build and run on your workstation, do

```bash
ng serve
```

You can access the application at `http://localhost:4200` on your workstation.

`ng serve` is set up to proxy API calls to `https://directorytest.wwu.edu`.
If you want to run the API locally, you'll have to change `target` in `proxy.conf.json` to `http://localhost:8080`.
Please do not commit this change, though.

## Building

You can perform the following tasks with the built-in Gradle wrapper.
This requires a JDK version between 1.8 and 15 on your workstation.

| Command                      | Description                        |
| ---------------------------- | ---------------------------------- |
| `./gradlew clean`            | Clean temporary build files        |
| `./gradlew build`            | Build application using `ng build` |
| `./gradlew dockerBuildImage` | Builds a docker image              |
| `./gradlew dockerPushImage`  | Pushes a docker image              |

## TODO

* "Loading" indicator

* Error reporting

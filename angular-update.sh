#!/usr/bin/env bash

# Every angular update is painful and always involves random flailing. This script
# is used as a basis for flailing in a repeatable way while trying to get an update
# to work. This is complcated by 'ng update's preference of starting each update
# step from a clean repo. This is problematical because even simple updates are
# are a multiple step process and I don't like commits that represent non-working
# code.

# The update process:
#
# 1. Start with master at a clean "last-known-good" commit.
#
# 2. Checkout a new angular-update branch.
#    git checkout -b angular-update
#
# 3. Start of the flailing loop: edit this file with the commands you hope will
#    do the upgrade and commit the change. Be sure to update the comment saying
#    what we're updating from and to.
#
# 4. Run the script. It discards the angular-update-attempt branch (if present)
#    and checks out a new one based on angular-update. Then, it attempts an update.
#
# 5. If the update fails, go back to 3 and try again.
#
# 6. When it finally works, squash and merge angular update into master

# Updating from Angular 12.0.3 to 12.2.11

UPDATE_BRANCH=angular-update
ATTEMPT_BRANCH=angular-update-attempt
MAIN_BRANCH=master

# set up colors
start_bold="$(tput bold)$(tput setaf 3)"
end_bold="$(tput sgr 0)"
start_warn="$(tput bold)$(tput setaf 1)"
end_warn="$(tput sgr 0)"
start_cmd="$(tput setaf 2)"
end_cmd="$(tput sgr 0)"

currentBranch() {
  git rev-parse --abbrev-ref HEAD
}

isBranch() {
  local branch="$1"; shift
  git show-ref --verify --quiet refs/heads/"$branch"
}

error () {
  local msg="$@"; shift
  : "${msg:?"error: missing message"}"
  echo "error: $msg" >&2
  exit 1
}

heading () {
    local msg="$@"
    : "${msg:?"prompt: missing message"}"
    echo "${start_bold}${msg}${end_bold}" < /dev/tty
}

prompt () {
    local msg="$@"
    : "${msg:?"prompt: missing message"}"
    [ -t 0 ] && read -p "${start_bold}${msg} (RETURN to continue)${end_bold}" < /dev/tty
    echo
    echo
}

warn() {
    local msg="$@"
    : "${msg:?"prompt: missing message"}"
    echo "${start_warn}${msg}${end_warn}" < /dev/tty
}

run () {
   local cmd=("$@")
    : "${cmd:?"run: missing command"}"
    echo "${start_cmd}$ ${cmd[@]}${end_cmd}"
    "${cmd[@]}" || error "failed: ${cmd[@]}"
}

# Make sure UPDATE_BRANCH exists and is at head of  MAIN_BRANCH
if isBranch "$UPDATE_BRANCH"; then
  # Check if MAIN_BRANCH is an ancestor of UPDATE_BRANCH
  if ! git merge-base --is-ancestor "$MAIN_BRANCH" "$UPDATE_BRANCH"; then
    echo "$MAIN_BRANCH is not an ancestor of $UPDATE_BRANCH. Either remove or rebase $UPDATE_BRANCH first" >&2
    exit 1
  fi
else
  git branch "$UPDATE_BRANCH" "$MAIN_BRANCH"
fi

# If we're on ATTEMPT_BRANCH, forcefully clean it up so we can delete it in a bit
if [ "$(currentBranch)" == "$ATTEMPT_BRANCH" ]; then
  git reset --hard HEAD
  git clean -d -f
fi

# Make sure the current branch does not have uncommitted or untracked changes
if [ -n "$(git status --porcelain)" ]; then
  echo "$(currentBranch) has uncommitted or untracked changes, clean it up first" >&2
  exit 1
fi

# Okay, we have our ducks lined up, check out UPDATE_BRANCH
git checkout "$UPDATE_BRANCH"

# Remove old ATTEMPT_BRANCH if it exists
if isBranch "$ATTEMPT_BRANCH"; then
  git branch -D "$ATTEMPT_BRANCH"
fi

# Create and checkout new ATTEMPT_BRANCH
git checkout -b "$ATTEMPT_BRANCH"

# ----------------------------------------------------------------------------
#
# Actual update commands follow
#
# Update to Angular 12.2.11

heading "Reset and install packages"
run rm -rf node_modules
run git restore package-lock.json
run git restore package.json
run npm install
prompt "Reset and install packages done"

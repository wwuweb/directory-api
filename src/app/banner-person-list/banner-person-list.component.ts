import { Component, TemplateRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { BannerDepartmentService } from '@app/banner-department-data';
import { BannerPerson, BannerPersonQuery, BannerPersonQueryBuilder, BannerPersonService } from '@app/banner-person-data';
import { ConfigurationService } from '@app/configuration/configuration.service';
import { AbstractDataListComponent, PagedDataSource } from 'paged-data';
import { Observable, of } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'directory-banner-person-list',
  templateUrl: './banner-person-list.component.html',
  styleUrls: ['./banner-person-list.component.scss']
})
export class BannerPersonListComponent extends AbstractDataListComponent<BannerPerson,BannerPersonQuery> {

  busy: boolean = false;
  data: BannerPerson[] = [];
  department?: string;
  recaptchaResponse?: string;
  _totalCount = 0;

  constructor(
    protected formBuilder: FormBuilder,
    protected override dataService: BannerPersonService,
    private bannerDepartmentService: BannerDepartmentService,
    private configurationService: ConfigurationService,
    private route: ActivatedRoute,
    private dialog: MatDialog,
) {
    super(dataService, new BannerPersonQueryBuilder(formBuilder));
  }

  get recaptchaSiteKey$() {
    return this.configurationService.configuration.pipe(map(c => c.recaptchaSiteKey));
  }

  resolved(response: any) {
    this.recaptchaResponse = response;
  }

  reveal(revealTemplate: TemplateRef<any>, person: BannerPerson) {
    this.dialog.open(revealTemplate, {ariaLabel: "Show email"})
      .afterClosed()
      .subscribe(() => {
        this.dataService.getData(person.id, this.recaptchaResponse)
          .subscribe(
            p => { person.email = p.email }
          )
      });
    this.recaptchaResponse = undefined;
  }

  get count() {
    return this.data.length;
  }

  get totalCount() {
    return this._totalCount;
  }

  ngOnInit() {
    this.route.paramMap.subscribe({ next: p => {
      let id = p.get('id');
      let department = id != null ? Number(id) : null;
      if (department != null) {
        this.bannerDepartmentService.getData(department).subscribe({
          next: d => { this.department = d.department; }
        });
      }
      else {
        this.department = undefined;
      }
      this.queryForm.get('department')?.setValue(id, {emitEvent: true});
      // Arbitrarily fetch first page from dataSource
      (this.dataSource.connect({viewChange: of({start: 0, end: PagedDataSource.pageSize - 1})}) as Observable<BannerPerson[]>)
        .subscribe({
          next: d => { this.data = d; }
        });
      this.dataSource.busy.pipe(debounceTime(300)).subscribe({next: b => this.busy = !!b });
      this.dataSource.size.subscribe({next: s => this._totalCount = s })
    }});
  }
}

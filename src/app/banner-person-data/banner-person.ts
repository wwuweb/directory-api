import { Data } from 'paged-data';

export class BannerPerson implements Data {
  id: number;
  lastName: string;
  firstName: string;
  title: string;
  phone: string;
  office: string;
  department: string;
  mailStop: string;
  email: string;
  homeDepartment: string;
  pronouns: string;

  constructor(bannerPerson: Partial<BannerPerson>) {
  this.id = bannerPerson?.id ?? 0;
  this.lastName = bannerPerson?.lastName ?? '';
  this.firstName = bannerPerson?.firstName ?? '';
  this.title = bannerPerson?.title ?? '';
  this.phone = bannerPerson?.phone ?? '';
  this.office = bannerPerson?.office ?? '';
  this.department = bannerPerson?.department ?? '';
  this.mailStop = bannerPerson?.mailStop ?? '';
  this.email = bannerPerson?.email ?? '';
  this.homeDepartment = bannerPerson?.homeDepartment ?? '';
  this.pronouns = bannerPerson?.pronouns ?? '';

  }
}

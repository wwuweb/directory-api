import { HttpParams } from "@angular/common/http";
import { Query } from "paged-data";

export class BannerPersonQuery implements Query {

  name?: string;
  department?: number;

  constructor(
    private query: Partial<BannerPersonQuery>
  ) {
    this.name = query.name;
    this.department = query.department;
  }

  private isBannerPersonQuery(other: Query | BannerPersonQuery): other is BannerPersonQuery {
    return "name" in other && "department" in other;
  }

  isEquals(other: Query): boolean {
      return this.isBannerPersonQuery(other) && this.name === other.name && this.department === other.department;
  }

  setQueryParameters(params: HttpParams): HttpParams {
    if (this.name != null) {
      params = params.set('name', this.name);
    }
    if (this.department != null) {
      params = params.set('department', this.department);
    }
    return params;
  }

}

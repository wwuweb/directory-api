import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "@env/environment";
import { AbstractDataService } from "paged-data";
import { Observable } from "rxjs";
import { BannerPerson, BannerPersonQuery } from ".";

@Injectable({
  providedIn: 'root'
})
export class BannerPersonService extends AbstractDataService<BannerPerson, BannerPersonQuery> {

  constructor(
    protected override httpClient: HttpClient
  ) {
    super(BannerPerson, httpClient);
  }

  protected get apiBaseUrl() {
    return environment.apiBaseUrl;
  }

  protected get domain() {
    return '/banner/person';
  }

  getData(id: number, recaptchaResponse?: string): Observable<BannerPerson> {
    let params = new HttpParams();
    if (recaptchaResponse != null) {
      params = params.set('g-recaptcha-response', recaptchaResponse);
    }
    return this.httpClient.get<BannerPerson>(this.apiBaseUrl + this.domain + '/' + id.toString(), {params: params});
  }

}

import { FormBuilder, FormGroup } from "@angular/forms";
import { AbstractQueryBuilder } from "paged-data";
import { BannerPersonQuery } from ".";

export class BannerPersonQueryBuilder extends AbstractQueryBuilder<BannerPersonQuery> {
  constructor(
    protected override formBuilder: FormBuilder
  ) {
    super(BannerPersonQuery, formBuilder);
  }

  buildQueryForm(builder: FormBuilder): FormGroup {
    return builder.group({
      name: [''],
      department: ['']
    });
  }

}

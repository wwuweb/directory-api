import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { Configuration } from './configuration';
import { DefaultConfiguration } from './default-configuration';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  private runtimeConfiguration?: Configuration;
  private runtimeConfigurationSubject: Subject<Configuration> = new Subject();

  constructor(
    private httpClient: HttpClient
  ) {
    httpClient
      .get<Configuration>('/configuration.json')
      .subscribe(c => {
        this.runtimeConfiguration = {...DefaultConfiguration, ...c};
        this.runtimeConfigurationSubject.next(this.runtimeConfiguration);
      });
  }

  get configuration(): Observable<Configuration> {
    if (this.runtimeConfiguration != null) {
      return of(this.runtimeConfiguration);
    }
    else {
      return this.runtimeConfigurationSubject;
    }
  }
}

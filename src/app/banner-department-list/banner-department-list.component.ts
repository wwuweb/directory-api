import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BannerDepartment, BannerDepartmentService } from '@app/banner-department-data';
import { AbstractDataListComponent, PagedDataSource, StringQuery, StringQueryBuilder } from 'paged-data';
import { debounceTime, Observable, of } from 'rxjs';

@Component({
  selector: 'directory-banner-department-list',
  templateUrl: './banner-department-list.component.html',
  styleUrls: ['./banner-department-list.component.scss']
})
export class BannerDepartmentListComponent extends AbstractDataListComponent<BannerDepartment,StringQuery> {

  busy: boolean = false;
  data: BannerDepartment[] = [];
  _totalCount = 0;

  constructor(
    private formBuilder: FormBuilder,
    protected dataService: BannerDepartmentService
  ) {
    super(dataService, new StringQueryBuilder(formBuilder));
  }

  get count() {
    return this.data.length;
  }

  get totalCount() {
    return this._totalCount;
  }

  ngOnInit() {
    // Arbitrarily fetch first page from dataSource
    (this.dataSource.connect({viewChange: of({start: 0, end: PagedDataSource.pageSize - 1})}) as Observable<BannerDepartment[]>)
      .subscribe({
        next: d => { this.data = d; }
      });
      this.dataSource.busy.pipe(debounceTime(300)).subscribe({next: b => this.busy = !!b });
      this.dataSource.size.subscribe({next: s => this._totalCount = s })
    }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BannerDepartmentListComponent } from './banner-department-list/banner-department-list.component';
import { BannerPersonListComponent } from './banner-person-list/banner-person-list.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/banner-person', pathMatch: 'full' },
  { path: 'banner-person', component: BannerPersonListComponent },
  { path: 'banner-department', component: BannerDepartmentListComponent },
  { path: 'banner-department/:id', component: BannerPersonListComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { AbstractDataService, StringQuery } from 'paged-data';
import { BannerDepartment } from '.';

@Injectable({
  providedIn: 'root'
})
export class BannerDepartmentService extends AbstractDataService<BannerDepartment, StringQuery> {

  constructor(
    protected override httpClient: HttpClient
  ) {
    super(BannerDepartment, httpClient);
  }

  protected get apiBaseUrl() {
    return environment.apiBaseUrl;
  }

  protected get domain() {
    return '/banner/department';
  }

}

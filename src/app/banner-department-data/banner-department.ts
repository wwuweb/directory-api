import { Data } from "paged-data";

export class BannerDepartment implements Data {
  id: number;
  department: string;

  constructor(bannerDepartment: Partial<BannerDepartment>) {
    this.id = bannerDepartment?.id ?? 0;
    this.department = bannerDepartment?.department ?? '';
  }
}

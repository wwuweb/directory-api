import { A11yModule } from '@angular/cdk/a11y';
import { LayoutModule } from '@angular/cdk/layout';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RecaptchaModule } from 'ng-recaptcha';
import { PagedDataModule } from 'paged-data';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BannerDepartmentListComponent } from './banner-department-list/banner-department-list.component';
import { BannerPersonListComponent } from './banner-person-list/banner-person-list.component';
import { NavComponent } from './nav/nav.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WesternFooterComponent } from './western-footer/western-footer.component';
import { WesternHeaderComponent } from './western-header/western-header.component';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    PageNotFoundComponent,
    BannerPersonListComponent,
    BannerDepartmentListComponent,
    WesternHeaderComponent,
    WesternFooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RecaptchaModule,
    FlexLayoutModule,
    A11yModule,
    LayoutModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressBarModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,
    ScrollingModule,
    PagedDataModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

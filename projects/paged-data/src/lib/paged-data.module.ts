import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { CanComponentDeactivateDialogComponent } from './can-component-deactivate';
import { SaveControlsComponent } from './save-controls';
import { DetailTabComponent } from './detail-tab/detail-tab.component';


@NgModule({
  declarations: [
    CanComponentDeactivateDialogComponent,
    SaveControlsComponent,
    DetailTabComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule
  ],
  exports: [
    CanComponentDeactivateDialogComponent,
    SaveControlsComponent
  ]
})
export class PagedDataModule { }

import { FormBuilder, FormGroup } from "@angular/forms";
import { defer, map, merge, Observable, of, tap } from "rxjs";
import { Query } from "./query";
import { QueryBuilder } from "./query-builder";

export abstract class AbstractQueryBuilder<Q extends Query> implements QueryBuilder<Q> {

  private _queryForm: FormGroup;

  constructor(
    protected queryType: new (value: Partial<Q>) => Q,
    protected formBuilder: FormBuilder
  ) {
    this._queryForm = this.buildQueryForm(formBuilder);
  }

  abstract buildQueryForm(builder: FormBuilder): FormGroup;

  queryChanges(): Observable<Q> {
    return merge(
      this._queryForm.valueChanges,
      defer(() => of(this._queryForm.value))
    ).pipe(map(q => this.queryFor(q)));
  }

  queryFor(q: Partial<Q>): Q {
    return new this.queryType(q);
  }

  get queryForm() {
    return this._queryForm;
  }

}

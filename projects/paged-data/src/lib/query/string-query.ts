import { HttpParams } from "@angular/common/http";
import { Query } from "./query";

export class StringQuery implements Query {

  queryString?: string;
  queryField?: string;

  constructor(
    private query: Partial<StringQuery>
  ) {
    this.queryString = query.queryString ?? ''
  }

  private isStringQuery(other: Query | StringQuery): other is StringQuery {
    return "queryString" in other && "queryField" in other;
  }

  isEquals(other: Query): boolean {
      return this.isStringQuery(other) && this.queryString === other.queryString && this.queryField == other.queryString;
  }

  setQueryParameters(params: HttpParams): HttpParams {
    return this.queryField != null && this.queryString != null ? params.set(this.queryField, this.queryString) : params;
  }

}

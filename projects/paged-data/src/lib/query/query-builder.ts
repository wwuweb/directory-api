import { FormBuilder, FormGroup } from "@angular/forms";
import { Observable } from "rxjs";
import { Query } from "./query";

export interface QueryBuilder<Q extends Query> {
  buildQueryForm(builder: FormBuilder): FormGroup;
  queryChanges(): Observable<Q>;
  queryFor(q: Partial<Q>): Q;
  get queryForm(): FormGroup;
}

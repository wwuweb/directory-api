import { HttpParams } from "@angular/common/http";

export interface Query {
  // Return true iff other produces the same query result as this
  isEquals(other: Query): boolean;
  setQueryParameters(params: HttpParams): HttpParams;
}

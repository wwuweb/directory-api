import { FormBuilder, FormGroup } from "@angular/forms";
import { AbstractQueryBuilder } from "./abstract-query-builder";
import { StringQuery } from "./string-query";

export class StringQueryBuilder extends AbstractQueryBuilder<StringQuery> {

  queryField = 'name';

  constructor (
    protected override formBuilder: FormBuilder
  ) {
    super(StringQuery, formBuilder);
  }

  buildQueryForm(builder: FormBuilder): FormGroup {
    return builder.group({
      queryString: ['']
    })
  }

  override queryFor(q: Partial<StringQuery>): StringQuery {
    const query = new this.queryType(q);
    query.queryField = this.queryField;
    return query;
  }

}

import { Data } from "./data";

export interface PagedDataList<T extends Data> {
  data: Array<T>;
  page: {
    size: number;
    totalElements: number;
    totalPages: number;
    number: number;
  }
}

import { Directive, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Data } from './data';

@Directive()
export abstract class AbstractDataTabComponent<T extends Data> implements OnInit {
  data?: T;

  constructor(
    protected route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.data.subscribe((data) => {
      if (data['data'] == null) {
        // TODO: put something here
      }
      else {
        this.data = data['data'];
      }
    });
  }
}

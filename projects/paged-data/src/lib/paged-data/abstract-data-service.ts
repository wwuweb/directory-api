import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Query } from '../query';
import { Data } from './data';
import { PagedDataList } from './paged-data-list';
import { PagedDataService } from './paged-data-service';

export abstract class AbstractDataService<D extends Data, Q extends Query> implements PagedDataService<D, Q> {

  constructor(
    protected type: new (value: Partial<D>) => D,
    protected httpClient: HttpClient
  ) { }

  protected abstract get apiBaseUrl(): string;
  protected abstract get domain(): string;

  protected url(query: Query): string {
    return this.apiBaseUrl + this.domain;
  }

  get new(): D {
    return new this.type({});
  }

  getDataList(query: Query, page: number, size: number) {
    const params = query.setQueryParameters(new HttpParams())
      .set('page', page.toString())
      .set('size', size.toString());

    return this.httpClient.get<PagedDataList<D>>(this.url(query), {params: params});
  }

  getData(id: number): Observable<D> {
    return this.httpClient.get<D>(this.apiBaseUrl + this.domain + '/' + id.toString());
  }

  putData(data: D): Observable<D> {
    return this.httpClient.put<D>(this.apiBaseUrl + this.domain + '/' + data.id.toString(), data);
  }
}

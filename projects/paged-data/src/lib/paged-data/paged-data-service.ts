import { Observable } from 'rxjs';
import { Query } from '../query';
import { Data } from './data';
import { PagedDataList } from './paged-data-list';


export interface PagedDataService<D extends Data, Q extends Query> {
  get new(): D;
  getDataList(query: Q, page: number, size: number): Observable<PagedDataList<D>>;
  getData(id: number): Observable<D>;
  putData(data: D): Observable<D>;
}

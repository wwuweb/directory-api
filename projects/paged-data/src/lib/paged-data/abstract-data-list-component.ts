import { Directive } from '@angular/core';
import { Query, QueryBuilder } from '../query';
import { AbstractDataService } from './abstract-data-service';
import { Data } from './data';
import { PagedDataSource } from './paged-data-source';

@Directive()
export abstract class AbstractDataListComponent<D extends Data, Q extends Query> {

  dataSource: PagedDataSource<D, Q>;

  constructor(
    protected dataService: AbstractDataService<D, Q>,
    protected queryBuilder: QueryBuilder<Q>
  ) {
    this.dataSource = new PagedDataSource<D, Q>(this.dataService, this.queryBuilder);
  }

  get queryForm() {
    return this.queryBuilder.queryForm;
  }
}

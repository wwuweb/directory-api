export interface Data {
  id: number;
}

// CachedData may be undefined if it is not present in the cache
export type CachedData<D extends Data> = D | undefined;
export type CacheDataArray<D extends Data> = Array<CachedData<D>>;

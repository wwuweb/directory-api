import { CollectionViewer, ListRange } from '@angular/cdk/collections';
import { DataSource } from '@angular/cdk/table';
import { combineLatestWith, concatAll, debounceTime, distinctUntilChanged, last, map, merge, Observable, of, Subject, Subscription, switchAll, tap } from 'rxjs';
import { CacheDataArray, Data, PagedDataService } from '.';
import { Query, QueryBuilder } from '../query';

/*
Notes on performance improvements

* Shouldn't call resetData() in PagedDataSource constructor. In connect when the result observable is set up,
* send messages to do the reset.
*/

export class PagedDataSource<D extends Data, Q extends Query> extends DataSource<D | undefined> {

  public static pageSize = 100;

  private maxPages = 100;
  private lastPage = this.maxPages;

  private dataService: PagedDataService<D, Q>;
  private queryStream: Observable<Q>;
  private previousQuery?: Q;

  private cachedData: CacheDataArray<D> = [];
  private cachedPages: Set<number> = new Set();

  private subscription = new Subscription();

  private _busy = new Subject<boolean>();
  private _size = new Subject<number>();

  constructor(dataService: PagedDataService<D, Q>, queryBuilder: QueryBuilder<Q>) {
    super();
    this.dataService = dataService;
    this.queryStream = queryBuilder.queryChanges();
  }

  connect(collectionViewer: CollectionViewer): Observable<CacheDataArray<D>> {
    return (collectionViewer.viewChange.pipe(
      combineLatestWith(this.queryStream),
      debounceTime(200),
      tap(() => this._busy.next(true)),
      map(([view, query]) => this.getData(query, view)),
      switchAll(),
      map(d => d.slice()),
      tap(() => this._busy.next(false))
    ));
  }

  disconnect() {
    this.subscription.unsubscribe();
  }

  get busy(): Observable<boolean> {
    return this._busy;
  }

  get size(): Observable<number> {
    return this._size.pipe(distinctUntilChanged(((prev, curr) => prev == curr)));
  }

  private page(index: number): number {
    return Math.floor(index / PagedDataSource.pageSize)
  }

  private getData(query: Q, view: ListRange) {
    if (this.previousQuery == null || this.previousQuery.isEquals(query)) {
      this.cachedData.length = 0;
      this.cachedPages.clear();
      this.lastPage = this.maxPages;
    }
    const startPage = this.page(view.start);
    const endPage = this.page(view.end);
    const pages: Observable<CacheDataArray<D>>[] =  []
    for (let i = startPage; i <= endPage; i++) {
      pages.push(this.getPage(query, i));
    }
    return merge(pages).pipe(concatAll(), last());
  }

  private getPage(query: Q, page: number): Observable<CacheDataArray<D>> {
    if (this.cachedPages.has(page) || page < 0 || page > this.lastPage) {
      // Page is in cache or page is out of bounds
      return of(this.cachedData);
    }
    else {
      return this.dataService.getDataList(query, page, PagedDataSource.pageSize)
        .pipe(
          tap(data => {
            this._size.next(data.page.totalElements);
            if (page == 0 || data.data.length > 0) {
              this.cachedPages.add(page);
              this.cachedData[page * PagedDataSource.pageSize] = undefined;
              this.cachedData.splice(page * PagedDataSource.pageSize, PagedDataSource.pageSize, ...(data.data));
            }
            this.lastPage = data.page.totalPages;
          }),
          map(data => this.cachedData)
        );
    }
  }
}

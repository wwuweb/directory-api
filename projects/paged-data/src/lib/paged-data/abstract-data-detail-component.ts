import { OnInit, OnDestroy, Directive } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Subject, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { SavableEvent, SavableService, SavableActions, SavableActionStatus } from '../savable';
import { CanComponentDeactivate } from '../can-component-deactivate';
import { AbstractDataService } from './abstract-data-service';
import { Data } from './data';
import { Query } from '../query';

@Directive()
export abstract class AbstractDataDetailComponent<D extends Data, Q extends Query> implements OnInit, OnDestroy, CanComponentDeactivate {

  data: D;
  detailForm: FormGroup;
  isEditing = false;
  savableEvent = new Subject<SavableEvent>();

  private savableActionSubscription?: Subscription;
  private savableChangesSubscription?: Subscription;

  constructor(
    protected formBuilder: FormBuilder,
    protected route: ActivatedRoute,
    protected dataService: AbstractDataService<D, Q>,
    protected savable: SavableService
  ) {
    this.data = dataService.new;
    this.detailForm = formBuilder.group(this.formControls());
  }

  protected abstract formControls(): { [key: string]: any; };

  ngOnInit() {
    this.savableActionSubscription = this.savable.actionEvents.subscribe((a: SavableActions) => {
      switch (a) {
        case SavableActions.Edit:
          this.edit();
          break;
        case SavableActions.Cancel:
          this.cancel();
          break;
        case SavableActions.Save:
          this.save();
          break;
      }
    });
    this.savableChangesSubscription = this.detailForm.statusChanges.pipe(
      filter(() => this.isEditing),
      map(() => ({[SavableActions.Save]: this.detailForm.dirty && this.detailForm.valid ? SavableActionStatus.Enabled : SavableActionStatus.Disabled}))
    ).subscribe((s) => this.savable.statusEvents.next(s));
    this.route.data.subscribe((data) => {
      if (data['data'] == null) {
        this.detailForm.reset(this.dataService.new);
        this.data = this.detailForm.value;
        this.edit();
      }
      else {
        this.data = data['data'];
        this.show();
      }
    });
  }

  ngOnDestroy() {
    this.savableActionSubscription?.unsubscribe();
    this.savableChangesSubscription?.unsubscribe();
  }

  canDeactivate() {
    return !this.isEditing || this.detailForm.pristine;
  }

  unloadNotification($event: any) {
    if (!this.canDeactivate()) {
      $event.returnValue = 'Leave this page with unsaved changes?';
    }
  }

  edit() {
    this.detailForm.enable();
    this.isEditing = true;
    this.savable.statusEvents.next({
      [SavableActions.Edit]: SavableActionStatus.Hidden,
      [SavableActions.Cancel]: SavableActionStatus.Enabled,
      [SavableActions.Save]: SavableActionStatus.Disabled
    });
  }

  show() {
    this.detailForm.patchValue(this.data);
    this.detailForm.disable();
    this.isEditing = false;
    this.savable.statusEvents.next({
      [SavableActions.Edit]: SavableActionStatus.Enabled,
      [SavableActions.Cancel]: SavableActionStatus.Hidden,
      [SavableActions.Save]: SavableActionStatus.Hidden
    });
  }

  save() {
    this.dataService.putData({...this.data, ...this.detailForm.value})
    .subscribe(data => {
      if (/*test for error*/false) {

      }
      else {
        this.data = this.detailForm.value;
        this.show();
      }
    })
  }

  cancel() {
    this.show();
  }
}

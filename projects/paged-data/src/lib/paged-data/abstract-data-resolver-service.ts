import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Data } from '.';
import { AbstractDataService } from './abstract-data-service';
import { Observable } from 'rxjs';
import { take, catchError } from 'rxjs/operators';
import { Query } from '../query';

class ResolveError extends Error {
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, ResolveError.prototype);
  }
}

export abstract class AbstractDataResolverService<D extends Data, Q extends Query> implements Resolve<D> {

  constructor(
    protected dataService: AbstractDataService<D, Q>,
    protected router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<D> | D {
    let id = route.paramMap.get('id');
    if (id === 'new') {
      return this.dataService.new;
    }
    return this.dataService.getData(Number(route.paramMap.get('id'))).pipe(
      take(1),
      catchError(() => {
        this.router.navigate(['..']);
        throw new ResolveError('record not found');
      })
    );
  }
}

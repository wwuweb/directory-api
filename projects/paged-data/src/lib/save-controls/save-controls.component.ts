import { Component, OnInit } from '@angular/core';
import { SavableService, SavableEvent, SavableActions, SavableActionStatus } from '../savable';
import { Subscription } from 'rxjs';

@Component({
  selector: 'paged-data-save-controls',
  templateUrl: './save-controls.component.html',
  styleUrls: ['./save-controls.component.scss']
})
export class SaveControlsComponent implements OnInit {

  private status: SavableEvent = {
    [SavableActions.Cancel]: SavableActionStatus.Hidden,
    [SavableActions.Save]: SavableActionStatus.Hidden
  }

  statusChangeSubscription?: Subscription;

  constructor(
    private savable: SavableService
  ) { }

  ngOnInit() {
    this.statusChangeSubscription = this.savable.statusEvents.subscribe((s: SavableEvent) => {
      this.status = {...this.status, ...s}
    })
  }

  get isEditEnabled() {
    return this.status[SavableActions.Edit] === SavableActionStatus.Enabled;
  }

  get isEditHidden() {
    return this.status[SavableActions.Edit] === SavableActionStatus.Hidden;
  }

  get isSaveEnabled() {
    return this.status[SavableActions.Save] === SavableActionStatus.Enabled;
  }

  get isSaveHidden() {
    return this.status[SavableActions.Save] === SavableActionStatus.Hidden;
  }

  get isCancelEnabled() {
    return this.status[SavableActions.Cancel] === SavableActionStatus.Enabled;
  }

  get isCancelHidden() {
    return this.status[SavableActions.Cancel] === SavableActionStatus.Hidden;
  }

  edit() {
    this.savable.edit();
  }

  save() {
    this.savable.save();
  }

  cancel() {
    this.savable.cancel();
  }
}

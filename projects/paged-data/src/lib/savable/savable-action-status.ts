export enum SavableActionStatus {
  Hidden,
  Disabled,
  Enabled
}

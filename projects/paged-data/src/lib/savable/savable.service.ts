import { Injectable } from '@angular/core';
import { Subject, Subscription, Observable, BehaviorSubject } from 'rxjs';
import { SavableActions } from './savable-actions';
import { SavableEvent } from './savable-event';

@Injectable({
  providedIn: 'root'
})
export class SavableService {

  private subscriptions = new Subscription();
  private _actionEvents = new Subject<SavableActions>();
  private _statusEvents = new Subject<SavableEvent>();

  constructor() { }

  get actionEvents() {
    return this._actionEvents;
  }

  get statusEvents() {
    return this._statusEvents;
  }

  edit() {
    this._actionEvents.next(SavableActions.Edit);
  }

  save() {
    this._actionEvents.next(SavableActions.Save);
  }

  cancel() {
    this._actionEvents.next(SavableActions.Cancel);
  }
}

import { SavableActions } from "./savable-actions";
import { SavableActionStatus } from './savable-action-status';

export type SavableEvent = {
  [P in SavableActions]?: SavableActionStatus;
}

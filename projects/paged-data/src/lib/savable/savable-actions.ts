export enum SavableActions {
  Edit,
  Cancel,
  Save
}

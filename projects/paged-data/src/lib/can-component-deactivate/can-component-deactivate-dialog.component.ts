import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-can-component-deactivate-dialog',
  templateUrl: './can-component-deactivate-dialog.component.html',
  styleUrls: ['./can-component-deactivate-dialog.component.scss']
})
export class CanComponentDeactivateDialogComponent {

  constructor(
    private dialogRef: MatDialogRef<CanComponentDeactivateDialogComponent>
  ) { }

  closeDialog(result: boolean) {
    this.dialogRef.close(result);
  }
}

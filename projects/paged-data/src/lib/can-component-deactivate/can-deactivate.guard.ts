import { Injectable } from "@angular/core";
import { CanDeactivate } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { CanComponentDeactivate } from './can-component-deactivate';
import { CanComponentDeactivateDialogComponent } from './can-component-deactivate-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {

  constructor (
    private dialog: MatDialog
  ) { }

  private deactivateDialog() {
    const dialogRef = this.dialog.open(CanComponentDeactivateDialogComponent, {
      width: '250px'
    });
    return dialogRef.afterClosed();
  }

  canDeactivate(component: CanComponentDeactivate) {
    return (!component || !component.canDeactivate || component.canDeactivate() ? true : this.deactivateDialog());
  }
}

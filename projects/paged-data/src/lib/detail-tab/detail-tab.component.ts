import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';

@Component({
  selector: 'paged-data-detail-tab',
  templateUrl: './detail-tab.component.html',
  styleUrls: ['./detail-tab.component.css']
})
export class DetailTabComponent implements OnInit {
  data?: Data;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe((data) => {
      if (data['data'] == null) {
        // TODO: put something here
      }
      else {
        this.data = data['data'];
      }
    })
  }

}
